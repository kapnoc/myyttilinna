{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Main where

import Apecs
import Apecs.Gloss
import System.Exit
import System.Random

import Debug.Trace

import Lib
import WorldGen

newtype SeedG =
  SeedG Int
  deriving (Show, Num)

instance Semigroup SeedG where
  (<>) = (+)

instance Monoid SeedG where
  mempty = SeedG 0

instance Component SeedG where
  type Storage SeedG = Global SeedG

-- CURSOR POS GLOBAL
newtype CursorPosition =
  CursorPosition (Int, Int)
  deriving (Show)

instance Semigroup CursorPosition where
  (<>) (CursorPosition (a, b)) (CursorPosition (c, d)) =
    CursorPosition (a + c, b + d)

instance Monoid CursorPosition where
  mempty = CursorPosition (0, 0)

instance Component CursorPosition where
  type Storage CursorPosition = Global CursorPosition

-- WORLD MAP GLOBAL
newtype WorldMapG =
  WorldMapG WorldMap
  deriving (Show)

instance Semigroup WorldMapG where
  (<>) (WorldMapG (WorldMap tiles1)) (WorldMapG (WorldMap tiles2)) =
    WorldMapG (WorldMap [])

instance Monoid WorldMapG where
  mempty = WorldMapG (WorldMap {tiles = []})

instance Component WorldMapG where
  type Storage WorldMapG = Global WorldMapG

makeWorld "World" [''WorldMapG, ''CursorPosition, ''SeedG, ''Camera]

initialize :: Int -> System World ()
initialize seed = do
  modify global $ \(CursorPosition c) -> CursorPosition (20, 30)
  modify global $ \(SeedG s) -> SeedG seed
  modify global $ \(WorldMapG w) ->
    WorldMapG $
    generateWorld
      seed
      WorldParams
        { heightMapParams =
            HeightMapParams
              { dimensions = (50, 80)
              , offset = (0, 0)
              , octaves = 5
              , xyScale = 20.0
              , zoom = 1
              , frequency = 1.0
              , lacunarity = 2.0
              , persistance = 0.5
              }
        }
  return ()

handleEvent :: Event -> System World ()
handleEvent (EventKey (SpecialKey KeyRight) Down _ _) =
  modify global $ \(CursorPosition (x, y)) -> CursorPosition (x + 1, y)
handleEvent (EventKey (SpecialKey KeyLeft) Down _ _) =
  modify global $ \(CursorPosition (x, y)) -> CursorPosition (x - 1, y)
handleEvent (EventKey (SpecialKey KeyUp) Down _ _) =
  modify global $ \(CursorPosition (x, y)) -> CursorPosition (x, y - 1)
handleEvent (EventKey (SpecialKey KeyDown) Down _ _) =
  modify global $ \(CursorPosition (x, y)) -> CursorPosition (x, y + 1)
handleEvent (EventKey (SpecialKey KeyEsc) Down _ _) = liftIO exitSuccess
handleEvent _ = return ()

step :: Float -> System World ()
step dT = return ()

square = Polygon [(0, 0), (0, -1), (-1, -1), (-1, 0), (0, 0)]

draw :: System World Picture
draw = do
  WorldMapG (WorldMap tiles) <- get global
  let tilePictures =
        map
          (\(WorldTile (x, y) altitude biome) ->
             translate (fromIntegral x * 11 - 700) (fromIntegral y * 11 - 350) .
             color (biomeColor biome) $
             scale 10 10 square) $
        concat tiles
  let world = pictures tilePictures
  (CursorPosition (curx, cury)) <- get global
  let cursor =
        translate (fromIntegral curx * 11 - 700) (fromIntegral cury * 11 - 350) .
        color red $
        scale 10 10 square
  SeedG seed <- get global
  let WorldChunk chunkTiles =
        generateChunk
          seed
          WorldParams
            { heightMapParams =
                HeightMapParams
                  { dimensions = (50, 80)
                  , offset = (0, 0)
                  , octaves = 5
                  , xyScale = 20.0
                  , zoom = 1
                  , frequency = 1.0
                  , lacunarity = 2.0
                  , persistance = 0.5
                  }
            }
          ChunkParams {position = (curx, cury), size = 10}
  let chunkTilePictures =
        map
          (\(WorldTile (x, y) altitude biome) ->
             translate (fromIntegral x * 11 - 100) (fromIntegral y * 11 - 350) .
             color (biomeColor biome) $
             scale 10 10 square) $
        concat chunkTiles
  let chunk = pictures chunkTilePictures
  return $ world <> cursor <> chunk

main :: IO ()
main = do
  randSeed <- randomIO :: IO Int
  print randSeed
  w <- initWorld
  runWith w $ do
    initialize randSeed
    play
      (InWindow "Myyttilinna" (1600, 900) (10, 10))
      black
      60
      draw
      handleEvent
      step
