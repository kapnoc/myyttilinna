{-# LANGUAGE DuplicateRecordFields #-}

module WorldGen
  ( generateWorld
  , heightMap
  , WorldParams(..)
  , HeightMapParams(..)
  , WorldMap(..)
  , WorldTile(..)
  , WorldChunk(..)
  , ChunkParams(..)
  , generateChunk
  , Biome(..)
  , biomeColor
  ) where

import Math.Noise.Modules.Perlin
import Math.Noise.NoiseModule

import Apecs.Gloss

import Debug.Trace

import Data.List
import Data.Maybe
import System.Random

data WorldTile =
  WorldTile
    { position :: (Int, Int)
    , altitude :: Double
    , biome :: Biome
    }
  deriving (Show)

data Biome
  = Ocean
  | Land
  | Mountain
  | Peak
  deriving (Show)

biomeColor Ocean = blue
biomeColor Land = green
biomeColor Mountain = dim $ dim $ dim orange
biomeColor Peak = white

biomeRange :: [(Biome, (Double, Double))]
biomeRange =
  [ (Ocean, (0.0, 0.40))
  , (Land, (0.40, 0.55))
  , (Mountain, (0.55, 0.65))
  , (Peak, (0.65, 1.0))
  ]

findTileBiome :: Double -> Biome
findTileBiome altitude = biome
  where
    (biome, _) =
      fromMaybe (Ocean, (1, 1)) $
      find (\(b, (min, max)) -> altitude >= min && altitude <= max) biomeRange

data ChunkParams =
  ChunkParams
    { position :: (Int, Int)
    , size :: Int
    }
  deriving (Show)

newtype WorldChunk =
  WorldChunk
    { tiles :: [[WorldTile]]
    }
  deriving (Show)

data Material
  = Soil
  | Rock
  | Water
  | Wood
  deriving (Show)

data ChunkTile =
  ChunkTile
    { position :: (Int, Int, Int)
    , material :: Material
    }
  deriving (Show)

generateChunk :: Int -> WorldParams -> ChunkParams -> WorldChunk
generateChunk seed (WorldParams heightMapParams) (ChunkParams (posx, posy) csize) =
  WorldChunk tiles
  where
    hMap =
      heightMap
        seed
        (heightMapParams
           { dimensions = (5 * csize, 5 * csize)
           , offset = (fromIntegral posx - 2, fromIntegral posy - 2)
           , zoom = csize
           })
    foMap =
      falloffMap
        FalloffMapParams
          { mapDimensions = dimensions heightMapParams
          , renderPosition = (fromIntegral posx - 2, fromIntegral posy - 2)
          , renderDimensions = (5, 5)
          , renderSteps = (csize, csize)
          }
    combinedMaps = zipWith (zipWith (*)) hMap foMap
    heightMapWithIndex = zip [0 ..] $ map (zip [0 ..]) combinedMaps
    tiles =
      map
        (\(i, x) ->
           (map $ \(j, y) ->
              WorldTile
                {position = (i, j), altitude = y, biome = findTileBiome y})
             x)
        heightMapWithIndex

newtype WorldParams =
  WorldParams
    { heightMapParams :: HeightMapParams
    }
  deriving (Show)

newtype WorldMap =
  WorldMap
    { tiles :: [[WorldTile]]
    }
  deriving (Show)

generateWorld :: Int -> WorldParams -> WorldMap
generateWorld seed (WorldParams heightMapParams) = WorldMap {tiles = tiles}
  where
    hMap = heightMap seed heightMapParams
    foMap =
      falloffMap
        FalloffMapParams
          { mapDimensions = dimensions heightMapParams
          , renderPosition = (0, 0)
          , renderDimensions = dimensions heightMapParams
          , renderSteps = (1, 1)
          }
    combinedMaps = zipWith (zipWith (*)) hMap foMap
    heightMapWithIndex = zip [0 ..] $ map (zip [0 ..]) combinedMaps
    -- TODO : add domain warping ?
    tiles =
      map
        (\(i, x) ->
           (map $ \(j, y) ->
              WorldTile
                {position = (i, j), altitude = y, biome = findTileBiome y})
             x)
        heightMapWithIndex

data FalloffMapParams =
  FalloffMapParams
    { mapDimensions :: (Int, Int)
    , renderPosition :: (Double, Double)
    , renderDimensions :: (Int, Int)
    , renderSteps :: (Int, Int)
    }
  deriving (Show)

falloffMap :: FalloffMapParams -> [[Double]]
falloffMap (FalloffMapParams (mapwidth, mapheight) (renderx, rendery) (renderwidth, renderheight) (rstepx, rstepy)) =
  [ [ 1.0 - flattenFalloff (max (abs x) (abs y))
  | y <-
      [ (((fromIntegral j / fromIntegral rstepy) + rendery) /
         (fromIntegral mapheight)) *
      2.0 -
      1.0
      | j <- [0 .. renderheight * rstepy - 1]
      ]
  ]
  | x <-
      [ (((fromIntegral i / fromIntegral rstepx) + renderx) /
         (fromIntegral mapwidth)) *
      2.0 -
      1.0
      | i <- [0 .. renderwidth * rstepx - 1]
      ]
  ]
  where
    flattenFalloff x = (x ** a) / ((x ** a) + (b - b * x) ** a)
    a = 5.0
    b = 2.2

data HeightMapParams =
  HeightMapParams
    { dimensions :: (Int, Int)
    , offset :: (Double, Double)
    , xyScale :: Double
    , zoom :: Int
    , octaves :: Int
    , frequency :: Double
    , lacunarity :: Double
    , persistance :: Double
    }
  deriving (Show)

-- randomList :: Int -> [Double]
-- randomList seed = randoms (mkStdGen seed) :: [Double]
heightMap :: Int -> HeightMapParams -> [[Double]]
heightMap seed (HeightMapParams (width, height) (offx, offy) xyScale zoom octaves frequency lacunarity persistance) =
  heightMap
  where
    generator =
      Perlin
        { perlinFrequency = frequency
        , perlinLacunarity = lacunarity
        , perlinOctaves = octaves
        , perlinPersistence = persistance
        , perlinSeed = seed
        }
    heightMap =
      [ [ (fromMaybe 0 (getValue generator (x, y, 0)) / 2) + 0.5
      | y <-
          [ (fromIntegral j / fromIntegral zoom + offy) / xyScale
          | j <- [0 .. height * zoom - 1]
          ]
      ]
      | x <-
          [ (fromIntegral i / fromIntegral zoom + offx) / xyScale
          | i <- [0 .. width * zoom - 1]
          ]
      ]
